import pandas as pd
import click
from yaml import load, SafeLoader
from sklearn.model_selection import train_test_split

df = pd.read_csv('data/kaggle/train.csv')

new_df = df[['GrLivArea', 'SalePrice']]


def foot_to_meter(foot: int) -> int:
    return round(foot / 10.76)


def usd_to_rub(usd: int) -> int:
    return round(usd * 80)


@click.command()
@click.option('--config_path', default='params/process_data.yaml')
def process_data(config_path):
    with open(config_path, encoding='utf-8') as f:
        config = load(f, Loader=SafeLoader)
    data_type = config['data_type']
    assert data_type in {'kaggle', 'cian'}

    out_data_train = config['out_data_train']
    out_data_test = config['out_data_test']

    if data_type == 'kaggle':
        new_df = process_kaggle(config)
    elif data_type == 'cian':
        new_df = process_cian(config)
    else:
        raise NameError('Unknown Data Type')

    print(new_df.head())
    train_df, test_df = train_test_split(new_df, random_state=0)
    train_df.to_csv(out_data_train)
    test_df.to_csv(out_data_test)


def process_cian(config):
    in_data = config['in_data_cian']
    columns = config['cian_columns'].split(' ')
    df = pd.read_csv(in_data, sep=";", encoding='windows-1251')
    return df[columns]


def process_kaggle(config):
    in_data = config['in_data_kaggle']
    columns = config['kaggle_columns'].split(' ')
    df = pd.read_csv(in_data)
    new_df = df[columns]
    new_df.loc[:, 'GrLivArea'] = new_df.loc[:, 'GrLivArea'].apply(foot_to_meter)
    new_df.loc[:, 'SalePrice'] = new_df.loc[:, 'SalePrice'].apply(usd_to_rub)
    return new_df.rename(columns={'GrLivArea': 'total_meters', 'SalePrice': 'Price'})


if __name__ == '__main__':
    process_data()
