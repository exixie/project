"""Preprocess cian data.
Takes params from config_path """
import glob
import os

import pandas as pd
import yaml
from sklearn.model_selection import train_test_split

config = {
    'in_data': 'data/cian',
    'train_out': 'data/processed/train.csv',
    'test_out': 'data/processed/test.csv'
}


def read_cian_data(config):
    in_data = config['in_data']
    all_files = glob.glob(os.path.join(in_data, "*.csv"))
    df_list = [pd.read_csv(f, delimiter=';') for f in all_files]
    df = pd.concat(df_list, ignore_index=True)

    return df[['total_meters', 'price']]


if __name__ == '__main__':
    df = read_cian_data(config)

    df['price'] = df['price'] / 1000
    df = df.drop(df[df.total_meters < 0].index)

    train_df, test_df = train_test_split(df, test_size=0.2)
    train_df.to_csv(config['train_out'])
    test_df.to_csv(config['test_out'])
