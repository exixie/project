import boto3
bucket_name = 'exixie'

load_dotenv('.env')

session = boto3.session.Session()
s3 = session.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net',
)

s3.upload_file('models/model_1.joblib', bucket_name, 'model_123456.joblib')

for key in s3.list_objects(Bucket=bucket_name)['Contents']:
    print(key['Key'])
