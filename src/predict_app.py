"""House price prediction service"""
from joblib import load
from flask import Flask, request
import yaml

app = Flask(__name__)
config = yaml.load(open('params/predict_app.yaml'), yaml.SafeLoader)
model = load(config['model_path'])


@app.route("/")
def home():
    """Dummy service"""
    f_1 = request.args.get("GrLivArea")
    f_2 = request.args.get("f2")
    f_3 = request.args.get("f3")
    f_1, f_2, f_3 = int(f_1), int(f_2), int(f_3)
    result = model.predict([[f_1]])[0]
    return f'{round(result/1000)} тыс.руб.'


if __name__ == "__main__":
    app.run()
