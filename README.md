# Project
Учебный проект для документации основ CI/CD

Сервис решает задачу предсказания цен на недвижимость

## Installation
To install project

```commondline
git clone https://gitlab.com/exixie/project
cd pabd
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Usage
Run protection app
```commondline
python src/predict_app.py
```

## URLs:
Kaggle Dataset для базового обучения модели можно скачать [здесь](https://storage.yandexcloud.net/pabd/kaggle.zip).

Инструкция по установке [docker](https://docs.docker.com/engine/install/ubuntu/)

Инструкция по установке [gitlab-runner](https://docs.gitlab.com/runner/install/linux-manually.html)

